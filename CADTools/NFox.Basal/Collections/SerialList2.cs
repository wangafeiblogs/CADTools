﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace NFox.Collections
{
    [Serializable]
    public class SerialList<TInfo, TValue> : SerialList<TValue>
    {

        public TInfo Info;
        public bool IsInfoSaved = true;

        public SerialList()
            : base()
        { }

        public SerialList(IEnumerable<TValue> lst)
            : base(lst)
        { }

        #region Xml

        protected override void WriteXml(XmlWriter writer)
        {
            XmlSerializer xs;
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            writer.WriteStartElement("Info");
            writer.WriteAttributeString("Saved", IsInfoSaved.ToString());
            if (IsInfoSaved)
            {
                xs = new XmlSerializer(typeof(TInfo));
                xs.Serialize(writer, Info, ns);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("Values");
            xs = new XmlSerializer(typeof(TValue));
            foreach (TValue item in this)
            {
                xs.Serialize(writer, item, ns);
            }
            writer.WriteEndElement();
        }

        protected override void ReadXml(XmlReader reader)
        {

            while (reader.Name != "Info")
            {
                reader.Read();
            }

            IsInfoSaved = Convert.ToBoolean(reader.GetAttribute("Saved"));
            reader.Read();

            XmlSerializer xs;
            if (IsInfoSaved)
            {

                if (Info != null && Info is ISerializableCollection)
                {
                    ((ISerializableCollection)Info).ReadXml(reader);
                }
                else
                {
                    xs = new XmlSerializer(typeof(TInfo));
                    Info = (TInfo)xs.Deserialize(reader);
                }

            }

            while (reader.Name != "Values")
            {
                reader.Read();
            }
            reader.Read();

            xs = new XmlSerializer(typeof(TValue));
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                TValue item = (TValue)xs.Deserialize(reader);
                if (item != null)
                {
                    Add(true, item);
                }
            };

            reader.Read();
            reader.Read();
            reader.Read();
        }

        #endregion

        #region Bin

        public override ISerializableCollection ReadFrom(string path)
        {

            SerialList<TInfo, TValue> lst =
                (SerialList<TInfo, TValue>)base.ReadFrom(path);

            if (lst != null)
            {
                Info = lst.Info;
            }
            return lst;
        }

        #endregion


    }
}
