﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NFox.Collections
{

    [Serializable]
    public class SerialList<T> : List<T>, IItems<T>, ISerializableCollection
    {

        public Action<T> ItemAdded { get; set; }
        public Action<T> ItemRemoving { get; set; }
        public Action<T> ItemChanged { get; set; }

        public SerialList()
            : base()
        { }

        public SerialList(IEnumerable<T> lst)
            : base(lst)
        { }

        #region IItems<T>

        public void SetEvents(Action<T> itemAdded, Action<T> itemRemoving, Action<T> itemChanged)
        {
            ItemAdded = itemAdded;
            ItemRemoving = itemRemoving;
            ItemChanged = itemChanged;
        }

        public void SetEvents(Action<T> itemAdded, Action<T> itemRemoving)
        {
            ItemAdded = itemAdded;
            ItemRemoving = itemRemoving;
            ItemChanged = null;
        }

        public void ForEach(Action<T, int> action)
        {
            int i = 0;
            foreach (T item in this)
            {
                action(item, i++);
            }
        }

        public void Update(T item)
        {
            if (ItemChanged != null)
            {
                ItemChanged(item);
            }
        }


        public void Add(bool allowEvents, T item)
        {
            base.Add(item);
            if (allowEvents && ItemAdded != null)
            {
                ItemAdded(item);
            }
        }

        public void AddRange(bool allowEvents, params T[] items)
        {
            foreach (T item in items)
            {
                Add(item);
            }
        }

        public void AddRange(bool allowEvents, IEnumerable<T> items)
        {
            if (allowEvents)
            {
                foreach (T item in items)
                {
                    Add(item);
                }
            }
            else
            {
                base.AddRange(items);
            }
        }

        public void Insert(bool allowEvents, int index, T item)
        {
            base.Insert(index, item);
            if (allowEvents && ItemAdded != null)
            {
                ItemAdded(item);
            }
        }

        public void InsertRange(bool allowEvents, int index, IEnumerable<T> lst)
        {
            base.InsertRange(index, lst);
            if (allowEvents && ItemAdded != null)
            {
                foreach (T item in lst)
                {
                    ItemAdded(item);
                }
            }
        }

        public bool Remove(bool allowEvents, T item)
        {
            if (allowEvents && ItemRemoving != null && this.Contains(item))
            {
                ItemRemoving(item);
            }
            return base.Remove(item);

        }

        public void RemoveAt(bool allowEvents, int index)
        {
            if (allowEvents && ItemRemoving != null && index > 0 && index < Count)
            {
                ItemRemoving(this[index]);
            }
            base.RemoveAt(index);
        }

        public void RemoveRange(bool allowEvents, int index, int count)
        {
            if (allowEvents)
            {
                for (int i = index; i < Count && i < index + count; i++)
                {
                    RemoveAt(i);
                }
            }
            else
            {
                base.RemoveRange(index, count);
            }
        }

        #endregion

        #region ISerialCollection

        #region Xml

        protected virtual void WriteXml(XmlWriter writer)
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            writer.WriteStartElement("Info");
            writer.WriteAttributeString("Saved", "false");
            writer.WriteEndElement();

            writer.WriteStartElement("Values");
            XmlSerializer xs = new XmlSerializer(typeof(T));
            foreach (T item in this)
            {
                xs.Serialize(writer, item, ns);
            }
            writer.WriteEndElement();
        }

        public virtual void WriteXml(string path)
        {
            using (XmlWriter writer = new XmlTextWriter(path, Encoding.UTF8))
            {

                writer.WriteStartDocument();
                writer.WriteStartElement("SerialList");
                WriteXml(writer);
                writer.WriteEndElement();

            }
        }

        protected virtual void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();

            while (reader.Name != "Values")
            {
                reader.Read();
            }
            reader.Read();

            XmlSerializer xs = new XmlSerializer(typeof(T));
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                T item = (T)xs.Deserialize(reader);
                if (item != null)
                {
                    Add(item);
                }
            };

            reader.Read();
            reader.Read();
        }

        public virtual void ReadXml(string path)
        {
            if (File.Exists(path))
            {
                using (XmlTextReader reader = new XmlTextReader(path))
                {
                    if (reader.NodeType == XmlNodeType.None)
                    {
                        reader.Read();
                    }

                    if (reader.NodeType == XmlNodeType.XmlDeclaration)
                    { 
                        reader.Read(); 
                    }

                    ReadXml(reader);
                }
            }
        }

        #region IXmlSerializable 成员

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {

            if (reader.NodeType == XmlNodeType.None || reader.NodeType == XmlNodeType.XmlDeclaration)
            {
                return;
            }

            ReadXml(reader);

        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            WriteXml(writer);
        }

        #endregion

        #endregion

        #region Bin

        class UBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {

                try
                {
                    return Type.GetType(typeName);
                }
                catch
                {
                    return Assembly.Load(assemblyName).GetType(typeName);
                }

            }
        }

        public virtual void WriteTo(string path)
        {
            using (Stream stream = File.Open(path, FileMode.Create))
            {
                BinaryFormatter bformatter = new BinaryFormatter();
                bformatter.Serialize(stream, this);
            }
        }

        public virtual ISerializableCollection ReadFrom(string path)
        {
            if (File.Exists(path))
            {
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    BinaryFormatter bformatter = new BinaryFormatter();
                    bformatter.Binder = new UBinder();
                    SerialList<T> lst = (SerialList<T>)bformatter.Deserialize(stream);
                    AddRange(lst);
                    return lst;
                }
            }
            return null;
        }

        #endregion

        #endregion

    }

}
