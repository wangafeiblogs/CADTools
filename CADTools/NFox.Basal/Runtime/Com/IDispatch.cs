﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace NFox.Runtime.Com
{
    [ComImport, Guid("00020400-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IDispatch
    {
        [System.Security.SecurityCritical]
        void GetTypeInfoCount(out uint pctinfo);

        [System.Security.SecurityCritical]
        void GetTypeInfo(uint iTInfo, int lcid, out ComTypes.ITypeInfo info);

        [System.Security.SecurityCritical]
        void GetIDsOfNames(
            ref Guid iid,
            [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeParamIndex = 2)]
            string[] names,
            uint cNames,
            int lcid,
            [Out]
            [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.I4, SizeParamIndex = 2)]
            int[] rgDispId);

        [PreserveSig, SecurityCritical]
        int Invoke(
            int mid, 
            Guid rid, 
            int lcid, 
            ComTypes.INVOKEKIND dwFlags, 
            ref ComTypes.DISPPARAMS dispParams, 
            out Variant resule, 
            IntPtr excepInfo, 
            out int argErr);
    }



}
