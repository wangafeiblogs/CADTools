﻿using System.Collections.Generic;
using System.Xml.Linq;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace NFox.Runtime.Com.Reflection
{

    /// <summary>
    /// 函数信息
    /// </summary>
    public class Function : MemberBase
    {

        internal Function(XElement xe)
            : base(xe)
        {
            foreach (var e in xe.Elements())
                Params.Add(new Parameter(e));
        }

        public Function(int id, string[] names, ComTypes.ITypeInfo info, ComTypes.FUNCDESC desc)
            : base(id, names[0])
        {

            Element = new Element(info, desc.elemdescFunc);
            var pars =
                Utils.GetObjects<ComTypes.ELEMDESC>(
                    desc.lprgelemdescParam,
                    desc.cParams);
            for (int i = 0; i < desc.cParams; i++)
            {
                Params.Add(
                    new Parameter(
                        i, names[i + 1], info, pars[i]));
            }
        }

        protected override string Category
        {
            get { return "Function"; }
        }

        public override XElement Node
        {
            get
            {
                var node = base.Node; 
                foreach (var par in Params)
                    node.Add(par.Node);
                return node;
            }
        }

        /// <summary>
        /// 函数参数集合
        /// </summary>
        public List<Parameter> Params { get; } = new List<Parameter>();

        public override string ToString()
        {
            var pars = string.Join(", ", Params);
            return $"{Element} {Name}({pars})";
        }

    }

}
