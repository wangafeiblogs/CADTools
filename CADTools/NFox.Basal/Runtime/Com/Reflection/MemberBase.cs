﻿using System;
using System.Xml.Linq;


namespace NFox.Runtime.Com.Reflection
{

    /// <summary>
    /// 成员变量信息基类
    /// </summary>
    public abstract class MemberBase
    {

        internal MemberBase(XElement node)
        {
            Name = node.Attribute("Name").Value;
            Index = int.Parse(node.Attribute("Id").Value);
            Element = new Element(node);
        }

        public MemberBase(int id, string name)
        {
            Index = id;
            Name = name;
        }

        /// <summary>
        /// 元素信息
        /// </summary>
        public Element Element { get; protected set; }

        /// <summary>
        /// 用于保存的Xml节点
        /// </summary>
        public virtual XElement Node
        {
            get
            {
                var node =
                    new XElement(Category,
                        new XAttribute("Name", Name),
                        new XAttribute("Id", Index));
                Element.SaveTo(node);
                return node;
            }
        }

        /// <summary>
        /// 类别名称
        /// </summary>
        protected abstract string Category { get; }

        /// <summary>
        /// 成员索引
        /// </summary>
        public int Index { get; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; }

        public override string ToString()
        {
            return $"{Element} {Name}";
        }


    }


}
