﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

namespace NFox.Cad.ExtendMethods
{

    public static class DBObjectEx
    {

        public static UpgradeOpenManager UpgradeOpenAndRun(this DBObject obj)
        {
            return new UpgradeOpenManager(obj);
        }

        public static IEnumerable<T> Clone<T>(this IEnumerable<T> objs) where T : RXObject
        {
            return objs.Select(obj => obj.Clone() as T);
        }

    }

    public class UpgradeOpenManager : IDisposable
    {

        DBObject _obj;
        bool _isNotifyEnabled;
        bool _isWriteEnabled;

        internal UpgradeOpenManager(DBObject obj)
        {
            _obj = obj;
            _isNotifyEnabled = _obj.IsNotifyEnabled;
            _isWriteEnabled = _obj.IsWriteEnabled;
            if (_isNotifyEnabled)
                _obj.UpgradeFromNotify();
            else if (!_isWriteEnabled)
                _obj.UpgradeOpen();
        }

        #region IDisposable 成员

        public void Dispose()
        {
            if (_isNotifyEnabled)
                _obj.DowngradeToNotify(_isWriteEnabled);
            else if (!_isWriteEnabled)
                _obj.DowngradeOpen();

        }

        #endregion
    }

}
