﻿using System.Collections.Generic;

namespace NFox.Cad.MTextTranslation
{

    public class RtfRangeNode : RtfNode, IEnumerable<RtfNode>
    {

        public short NumOfLimitNodes
        { get; set; }

        List<RtfNode> _lst = new List<RtfNode>();

        public RtfRangeNode()
        {
            _nodeClassType = RtfNodeClassType.Range;
            _key = -1;
        }

        internal RtfRangeNode(string contents)
        {
 
        }

        public void Add(RtfNode node)
        {
            node.Owner = this;   
            _lst.Add(node);
        }

        public void Add(string textString)
        {
            Add(new RtfTextNode(textString));
        }

        public void Add(RtfLimitNodeType nodeType)
        {
            NumOfLimitNodes ++;
            Add(new RtfLimitNode(nodeType));
        }

        public void Add(RtfScriptNodeType nodeType, string superScript, string subScript)
        {
            Add(new RtfScriptNode(nodeType, superScript, subScript));
        }

        public void Add(RtfSimpleNodeType nodeType, string specString)
        {
            Add(new RtfSimpleNode(nodeType, specString));
        }

        public void Add(RtfSpecCodeNodeType nodeType)
        {
            Add(new RtfSpecCodeNode(nodeType));
        }

        public override string Contents
        {
            get
            {
                string s = "";
                foreach (RtfNode node in _lst)
                {
                    s += node.Contents;
                }
                if (Owner != null || NumOfLimitNodes > 0)
                {
                        s = "{" + s + "}";
                }
                return s;
            }
        }

        #region IEnumerable<RtfNode> 成员

        IEnumerator<RtfNode> IEnumerable<RtfNode>.GetEnumerator()
        {
            return _lst.GetEnumerator();
        }

        #endregion

        #region IEnumerable 成员

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _lst.GetEnumerator();
        }

        #endregion
    }
}
