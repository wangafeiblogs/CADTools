﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace NFox.Cad.Runtime
{
    public class AcadVersion
    {

        public int Major
        { private set; get; }

        public int Minor
        { private set; get; }
        
        public double ProgId
        {
            get { return double.Parse($"{Major}.{Minor}"); }
        }

        public string ProductName
        { private set; get; }

        public string ProductRootKey
        { private set; get; }


        static string _pattern = @"Autodesk\\AutoCAD\\R(\d+)\.(\d+)\\.*?";


        private static List<AcadVersion> _versions;
        public static List<AcadVersion> Versions
        {
            get
            {

                if (_versions == null)
                {

                    string[] copys =
                       Registry.LocalMachine
                       .OpenSubKey(@"SOFTWARE\Autodesk\Hardcopy")
                       .GetValueNames();
                    _versions = new List<AcadVersion>();
                    foreach (var rootkey in copys)
                    {
                        if (Regex.IsMatch(rootkey, _pattern))
                        {
                            var gs = Regex.Match(rootkey, _pattern).Groups;
                            var ver =
                                new AcadVersion
                                {

                                    ProductRootKey = rootkey,
                                    ProductName =
                                        Registry.LocalMachine
                                        .OpenSubKey("SOFTWARE")
                                        .OpenSubKey(rootkey)
                                        .GetValue("ProductName")
                                        .ToString(),

                                    Major = int.Parse(gs[1].Value),
                                    Minor = int.Parse(gs[2].Value),

                                };

                            _versions.Add(ver);

                        }
                    }
                }
                return _versions;
            }
        }

        public static AcadVersion FromApp(object app)
        {
            string acver =
                app.GetType()
                    .InvokeMember(
                    "Version",
                    BindingFlags.GetProperty,
                    null,
                    app,
                    new object[0]).ToString();

            var gs = Regex.Match(acver, @"(\d+)\.(\d+).*?").Groups;
            int major = int.Parse(gs[1].Value);
            int minor = int.Parse(gs[2].Value);
            foreach (var ver in Versions)
            {
                if (ver.Major == major && ver.Minor == minor)
                    return ver;
            }

            return null;

        }

        public override string ToString()
        {
            return
                $"名称:{ProductName}\n版本号:{ProgId}\n注册表位置:{ProductRootKey}";
        }


    }
}
