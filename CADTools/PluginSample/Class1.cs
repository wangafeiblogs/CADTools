﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using CADTools;
using NFox.Cad.ExtendMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Autodesk.Windows;
using PluginSample.Properties;
namespace PluginSample
{
    /// <summary>
    /// 插件示例
    /// 可以很方便的将已经写好的插件转换为cadtools支持的插件
    /// 只要三步：
    /// 1、导出属性
    /// 2、继承IPlugin接口
    /// 3、实现IPlugin接口的MenuTab属性
    /// 这些做完，生成，然后把dll复制到cadtools的plugins目录，就可以了
    /// 这些做完就ok了，插件会自动将MenuTab定义的菜单生成ribbon和menu
    /// </summary>

    [Export(typeof(IPlugin))] //1、导出属性，用于被主程序注册为插件
    public class Test1 : PluginBase //2、这里要继承插件接口
    {
        /// <summary>
        /// 定义MenuTab属性。
        /// </summary>
        public override RibbonTab MenuTab => new Tab("插件平台")
                {
                    new Panel("测试")
                    {
                        {"测试","line ", Resources.setup}
                    },
                    
                    new Panel("hahaha")
                    {
                        new SplitButton("heihei")
                        {
                            {"hellp","line",Resources.setup},
                            {"hellr","line",Resources.setup},
                            {"hellt","line",Resources.setup},
                            {"helly","line",Resources.setup},
                        },
                        new RowPanel("hoho")
                        {
                            {"hellz","line",Resources.setup},
                            {"hellx","line",Resources.setup},
                            {BreakType.Row },
                            {"hellc","line",Resources.setup},
                            {"hellv","line",Resources.setup},
                        }
                        
                    }
                    
                };//3、定义MenuTab属性

        /// <summary>
        /// 这里就是插件的正常命令函数的编写，和正常的没啥区别
        /// </summary>
        [CommandMethod("test11")]
        public static void test11()
        {
            Ed.WriteMessage("this is the 1\n");

        }
    }

}
