﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;

using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
using NFox.Cad.ExtendMethods;
namespace CADTools
{

    /// <summary>
    /// 定义插件基础的抽象类
    /// 目前还没有特别的需要给子类利用的抽象方法和非抽象方法，只有定义几个静态的属性供子类使用
    /// </summary>
    public abstract class PluginBase : IPlugin
    {
        public static Editor Ed => Application.DocumentManager.MdiActiveDocument.Editor;
        public static Document Doc => Application.DocumentManager.MdiActiveDocument;
        public static Database Db => Doc.Database;
        public static RibbonControl RibbonControl => ComponentManager.Ribbon;
      
        public static DirectoryInfo CurrentPath => new DirectoryInfo(Assembly.GetExecutingAssembly().Location.ToString());

        public abstract RibbonTab MenuTab { get; }
    }
}
