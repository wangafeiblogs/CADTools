﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using Autodesk.Windows;
using System.Windows.Media;
using System.Drawing;
using NFox.Cad.ExtendMethods;
using System.Windows.Media.Imaging;

namespace CADTools
{
    public class Panel : RibbonPanel, IEnumerable<RibbonItem>
    {
        
        /// <summary>
        /// 初始化Ribbon面板
        /// </summary>
        /// <param name="title">面板的名字，默认生成$"ID_Panel_{title}"的ID</param>
        public Panel(string title)
        {
            Id = $"ID_Panel_{title}";
            
            Source = new RibbonPanelSource
            {
                Title = title,
                Id = $"ID_PanelSource_{title}"
            };
            
        }

        public void Add(RibbonItem item)
        {
            Source.Items.Add(item);
        }
        public void Add(BreakType type)
        {
            switch (type)
            {
                case BreakType.Panel:
                    Source.Items.Add(new RibbonPanelBreak());
                    break;
                case BreakType.Row:
                    Source.Items.Add(new RibbonRowBreak());
                    break;
                default:
                    break;
            }
        }
        public void Add(RibbonSeparatorStyle style)
        {
            Source.Items.Add(new RibbonSeparator { SeparatorStyle = style });
        }
        public void Add(string name, string cmd, Bitmap image = null, RibbonToolTip toolTip = null,
            RibbonItemSize size = RibbonItemSize.Large, Orientation orient = Orientation.Vertical)
        {
            BitmapImage imagetmp;
            if (image == null)
            {
                imagetmp = null;
            }
            else
            {
                imagetmp = image.ToBitmapImage();
            }
            
            Source.Items.Add(
                new RibbonButton
                {
                    Name = name,//按钮的名称
                    Text = name,
                    Id = $"ID_Button_{name}",
                    ShowText = true, //显示文字

                    Size = size, //按钮尺寸
                    Orientation = orient, //按钮排列方式
                    CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                    CommandParameter = cmd + " ",
                    ShowImage = true, //显示图片
                    ToolTip = toolTip,
                    Image = imagetmp,
                    LargeImage = imagetmp
                });
        }

        public IEnumerator<RibbonItem> GetEnumerator()
        {
            return Source.Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Source.Items.GetEnumerator();
        }
    }
}
