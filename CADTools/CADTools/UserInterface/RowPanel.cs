﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using Autodesk.Windows;
using System.Windows.Media;
using NFox.Cad.ExtendMethods;
using System.Drawing;
using System.Windows.Media.Imaging;
namespace CADTools
{
    public class RowPanel : RibbonRowPanel, IEnumerable<RibbonItem>
    {
        
        /// <summary>
        /// 初始化行面板，主要用于一列按钮的排布
        /// </summary>
        /// <param name="text">面板的名字</param>
        public RowPanel(string text)
        {
            this.Text = text;
            Id = $"ID_Row_{text}";
            
        }
        public void Add(RibbonItem item)
        {
            Items.Add(item);
        }
        public void Add(BreakType type)
        {
            if (type == BreakType.Row)
            {
                Items.Add(new RibbonRowBreak());
            }

        }
        public void Add(RibbonSeparatorStyle style)
        {
            Items.Add(new RibbonSeparator { SeparatorStyle = style });
        }
        public void Add(string name, string cmd, Bitmap image = null, RibbonToolTip toolTip = null,
            RibbonItemSize size = RibbonItemSize.Large, Orientation orient = Orientation.Vertical)
        {
            BitmapImage imagetmp;
            if (image == null)
            {
                imagetmp = null;
            }
            else
            {
                imagetmp = image.ToBitmapImage();
            }
            Items.Add(
                new RibbonButton
                {
                    Name = name,//按钮的名称
                    Text = name,
                    Id = $"ID_Button_{name}",
                    ShowText = true, //显示文字

                    Size = size, //按钮尺寸
                    Orientation = orient, //按钮排列方式
                    CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                    CommandParameter = cmd + " ",
                    ShowImage = true, //显示图片
                    ToolTip = ToolTip,
                    Image = imagetmp,
                    LargeImage = imagetmp
                });
        }

        public IEnumerator<RibbonItem> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }
}
